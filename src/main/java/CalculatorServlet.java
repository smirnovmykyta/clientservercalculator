import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/calculator")
public class CalculatorServlet extends HttpServlet{

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException, IOException {
        String strA = request.getParameter("a");
        String strB = request.getParameter("b");
        String strOperator = request.getParameter("op");
        double valueA = 0;
        double valueB = 0;
        double result = 0;
        boolean noError = true;


        try {
            valueA = Double.parseDouble(strA);
            valueB = Double.parseDouble(strB);
            if (strOperator.equals("+")) {
                result = functionSum(valueA, valueB);
            } else if (strOperator.equals("-")) {
                result = functionDif(valueA, valueB);
            } else if (strOperator.equals("*")) {
                result = functionMul(valueA, valueB);
            } else if (strOperator.equals("/") && (valueB != 0)) {
                result = functionDiv(valueA, valueB);
            } else {
                noError = false;
            }
        } catch (Exception ex) {
            noError = false;
        }
        if (noError) {
            doSetResult(response, result);
            return;
        }
    }

    protected void doSetResult(HttpServletResponse response, double result) throws UnsupportedEncodingException, IOException {
        String reply = "{\"error\":0,\"result\":" + Double.toString(result) + "}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doSetError(HttpServletResponse response) throws UnsupportedEncodingException, IOException {
        String reply = "{\"error\":1}";
        response.getOutputStream().write(reply.getBytes("UTF-8"));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected double functionSum(double a, double b) {
        return a + b;
    }

    protected double functionDif(double a, double b) {
        return a - b;
    }

    protected double functionMul(double a, double b) {
        return a * b;
    }

    protected double functionDiv(double a, double b) {
        return a / b;
    }
}

